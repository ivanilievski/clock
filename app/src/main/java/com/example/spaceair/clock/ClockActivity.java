package com.example.spaceair.clock;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextClock;

public class ClockActivity extends AppCompatActivity {

    private TextClock mTextClock1;
    private TextClock mTextClock2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clock);

        mTextClock1 = (TextClock) findViewById(R.id.textClock);
        mTextClock2 = (TextClock) findViewById(R.id.textClock2);

    }
}
